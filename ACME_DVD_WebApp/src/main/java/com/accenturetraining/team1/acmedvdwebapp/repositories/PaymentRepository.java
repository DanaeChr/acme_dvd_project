package com.accenturetraining.team1.acmedvdwebapp.repositories;

import com.accenturetraining.team1.acmedvdwebapp.model.Customer;
import com.accenturetraining.team1.acmedvdwebapp.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
    @Query(nativeQuery = true, value = "SELECT c.first_name, c.last_name, SUM(p.amount) " +
            "FROM payments p " +
            "INNER JOIN customers c ON p.customer_id = c.id" +
            "GROUP BY c.id " +
            "ORDER BY SUM(p.amount) DESC, c.last_name, c.first_name")
    List<Customer> findAllByAmountAAndCustomer();
}
