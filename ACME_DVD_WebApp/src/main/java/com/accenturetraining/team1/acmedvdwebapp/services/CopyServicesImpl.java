package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.model.Copy;
import com.accenturetraining.team1.acmedvdwebapp.model.CopyCategory;
import com.accenturetraining.team1.acmedvdwebapp.model.Film;
import com.accenturetraining.team1.acmedvdwebapp.repositories.CopyRepository;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.CopyIsAvailableException;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.FilmIsUnavailableException;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.CopyNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class CopyServicesImpl implements CopyServices {
    @Autowired
    CopyRepository copyRepository;
    @Autowired
    FilmServices filmServices;
    @Autowired
    CopyCategoryServices copyCategoryServices;


    @Override
    public long registerNewCopy(long filmId, int copyCategoryId) {
        Copy copy = new Copy();
        Film film = filmServices.getFilmById(filmId);
        copy.setFilm(film);
        copy.setRented(false);
        CopyCategory copyCategory = copyCategoryServices.findCopyCategoryById(copyCategoryId);
        copy.setCopyCategory(copyCategory);
        copy = copyRepository.saveAndFlush(copy);
        return copy.getId();
    }

    @Override
    public Copy updateCopy(Copy copy) {
        copy = copyRepository.saveAndFlush(copy);
        return copy;
    }

    @Override
    public Copy findCopyById(long copyId) {
        Copy copy = copyRepository.getById(copyId).orElseThrow(() -> new CopyNotFoundException(copyId));
        return copy;
    }


    @Override
    public void validateCopyUnavailability(long copyId) throws CopyIsAvailableException {
        Copy copy = copyRepository.getById(copyId).get();
        if (!copy.isRented()) {
            throw new CopyIsAvailableException(copyId);
        }
    }


    @Override
    public Copy checkForAvailableCopyOfFilm(long filmId, int copyCategoryId)
            throws FilmIsUnavailableException {
        filmServices.getFilmById(filmId);
        Copy availableCopyOfFilm = copyRepository
                .getFirstByIsRentedFalseAndFilmIdAndCopyCategoryId(filmId, copyCategoryId)
                .orElse(null);
        
        if (availableCopyOfFilm == null) {
            throw new FilmIsUnavailableException(filmId);
        }

        return availableCopyOfFilm;
    }


    @Override
    public boolean checkForFilmAvailability(long filmId) {
        Copy availableCopyOfFilm = copyRepository
                .getFirstByIsRentedFalseAndFilmId(filmId)
                .orElse(null);
        
        if (availableCopyOfFilm != null) {
            return true;
        }
        return false;
    }
}
