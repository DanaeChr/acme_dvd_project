package com.accenturetraining.team1.acmedvdwebapp.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "copy_categories")
public class CopyCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String categoryName;
    private double initialFee;
    private double dailyLateFee;
    private double firstPenalty;
    private double secondPenalty;
    
    @ToString.Exclude
    @OneToMany(mappedBy = "copyCategory")
    private List<Copy> copies;
}
