package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data Transfer Object
 * Used for rental reports and for searching films dy dates as a request body
 */

@Data
@NoArgsConstructor
public class BetweenDatesDTO {
    String dateFromText;
    String dateToText;
}
