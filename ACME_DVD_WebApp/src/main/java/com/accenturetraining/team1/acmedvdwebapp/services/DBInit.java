package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.RentalReturnDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.*;
import com.accenturetraining.team1.acmedvdwebapp.repositories.ActorRepository;
import com.accenturetraining.team1.acmedvdwebapp.repositories.CopyCategoryRepository;
import com.accenturetraining.team1.acmedvdwebapp.repositories.FilmRepository;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerAccountIsInactive;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerDuplicateException;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerInDebtException;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.CopyIsAvailableException;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.FilmIsUnavailableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class DBInit implements ApplicationListener<ApplicationReadyEvent> {
    @Autowired
    GeneralSettingsServices globalSettingsServices;
    @Autowired
    FilmServices filmServices;
    @Autowired
    CopyServices copyServices;
    @Autowired
    CopyCategoryRepository copyCategoryRepository;
    @Autowired
    RentalServices rentalServices;
    @Autowired
    FilmRepository filmRepository;
    @Autowired
    ActorRepository actorRepository;
    @Autowired
    ActorServices actorServices;
    @Autowired
    CustomerServices customerServices;
    @Autowired
    GeneralSettingsServices generalSettingsServices;
    @Autowired
    FilmDataDeserializer filmDataDeserializer;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
//      initializeEverything();
//
//       try {
//           initializeReturns();
//        }
//       catch (CopyIsAvailableException e) {
//           e.printStackTrace();
//        }
//       try {
//           initializeReturns();
//       }
//        catch (CopyIsAvailableException e) {
//           e.printStackTrace();
//       }
    }
    
    public void initializeEverything() {
        // Initialize CopyCategories
        log.debug("INITIALIZING CATEGORIES");
        List<CopyCategory> copyCategories = initializeCopyCategories();
        log.debug("INITIALIZED CATEGORIES");
    
        // Initialize Customers
        List<Customer> customers = new ArrayList<>();
        log.info("INITIALIZING CUSTOMERS");
        try {
            customers = initializeCustomers();
        }
        catch (CustomerDuplicateException e) {
            e.printStackTrace();
        }
        log.info("INITIALIZED CUSTOMERS");
    
        GeneralSettings generalSettings = initializeGeneralSettings();
    
    
        // Initialize films, genres and actors.
        List<Film> films = new ArrayList<>();
        try {
            films = filmDataDeserializer.deserializeFilmDataFromJson();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    
        // Initializing copies
        log.info("INITIALIZING COPIES");
        for(Film film : films)
        {
            for(int i = 0; i < 4; i++) {
                copyServices.registerNewCopy(film.getId(), 1);
            }
            for (int i = 0; i < 2; i++) {
                copyServices.registerNewCopy(film.getId(), 2);
            }
        }
        log.info("INITIALIZED COPIES");
    
        // Initializing rentals
        log.info("INITIALIZING RENTALS");
        List<Rental> rentals;
        try {
            initializeRentals();
        }
        catch (CustomerInDebtException e) {
            e.printStackTrace();
        }
        catch (FilmIsUnavailableException e) {
            e.printStackTrace();
        }
        catch (CustomerAccountIsInactive customerAccountIsInactive) {
            customerAccountIsInactive.printStackTrace();
        }
        log.info("INITIALIZED RENTALS");
    }
    
   

    public List<CopyCategory> initializeCopyCategories() {
        List<CopyCategory> copyCategories = new ArrayList<>();

        // DVD copy category
        CopyCategory dvd = new CopyCategory();
        dvd.setCategoryName("DVD");
        dvd.setInitialFee(0.5);
        dvd.setDailyLateFee(0.25);
        dvd.setFirstPenalty(10);
        dvd.setSecondPenalty(20);
        copyCategoryRepository.save(dvd);
        // Blu-ray copy category
        CopyCategory bluray = new CopyCategory();
        bluray.setCategoryName("Blu-Ray");
        bluray.setInitialFee(1);
        bluray.setDailyLateFee(0.5);
        bluray.setFirstPenalty(15);
        bluray.setSecondPenalty(25);
        copyCategoryRepository.save(bluray);

        copyCategories.add(dvd);
        copyCategories.add(bluray);
        return copyCategories;
    }

    public List<Customer> initializeCustomers() throws CustomerDuplicateException {
        List<Customer> customers = new ArrayList<>();

        Customer customer1 = customerServices.addCustomer("Manolis", "Manolopoulos", "AA274654",
                "theodoridou 73,Nea Iwnia", "6987439021", "theodoridou@gmail.com");
        Customer customer2 = customerServices.addCustomer("Vaggelis", "Rapidis", "AA483761",
                "Rodou 85, Kifisia", "6945620416", "v.rapidis@gmail.com");
        Customer customer3 = customerServices.addCustomer("Nikos", "Anastasiadhs", "AB871544",
                "Hrakleous 23, Kifisia", "6983450187", "anastasiadhs@gmail.com");
        Customer customer4 = customerServices.addCustomer("Aristhdhs", "Kiritshs", "BB896710",
                "Makrhs 19, Kifisia", "6973219572", "kiritshs@gmail.com");
        Customer customer5 = customerServices.addCustomer("Aggelikh", "Dhmhtriou", "AP385775",
                "Papakiriskou 4, Kifisia", "6932458172", "dhmhtriou@gmail.com");
        Customer customer6 = customerServices.addCustomer("Tatiana", "Nikolaou", "AP856377",
                "Galinou 32, Kifisia", "6932456632", "tatiana.nikolaou@gmail.com");
        Customer customer7 = customerServices.addCustomer("Maria", "Tatidi", "AP885277",
                "Nirea 19, Kifisia", "6931238632", "maria.tatidi@gmail.com");
        Customer customer8 = customerServices.addCustomer("Naomi", "Euristhenous", "AP585277",
                "Galinou 54, Kifisia", "6931232232", "naomi.eur@gmail.com");
        Customer customer9 = customerServices.addCustomer("Anastasios", "Tatidi", "AN883477",
                "Nirea 19, Kifisia", "6931238632", "maria.tatidi@gmail.com");
        Customer customer10 = customerServices.addCustomer("Maria", "Tatidi", "AP71277",
                "Nirea 19, Kifisia", "6931238632", "maria.tatidi@gmail.com");
        
        customers.add(customer1);
        customers.add(customer2);
        customers.add(customer3);
        customers.add(customer4);
        customers.add(customer5);
        customers.add(customer6);
        customers.add(customer7);
        customers.add(customer8);
        customers.add(customer9);
        customers.add(customer10);
        
        return customers;
    }

    public List<Rental> initializeRentals() throws CustomerInDebtException, FilmIsUnavailableException, CustomerAccountIsInactive {
        List<Rental> rentals = new ArrayList<>();
        log.info("Renting copies");
        rentalServices.rentCopy(1, 325, 1);
        rentalServices.rentCopy(2, 205, 1);
        rentalServices.rentCopy(3, 583, 2);
        rentalServices.rentCopy(4, 234, 1);
        rentalServices.rentCopy(4, 682, 2);
        rentalServices.rentCopy(5, 194, 1);
        rentalServices.rentCopy(5, 894, 1);
        rentalServices.rentCopy(4, 186, 1);
        rentalServices.rentCopy(1, 452, 1);

        return rentals;
    }

    public List<Rental> initializeReturns() throws CopyIsAvailableException {
        List<Rental> rentals = new ArrayList<>();
        LocalDateTime returnDate1 = LocalDateTime.of(2019, 4, 13, 12, 00);
        RentalReturnDTO rentalReturnDTO = rentalServices.returnCopy(3497, returnDate1);
        log.debug(rentalReturnDTO.toString());
        rentalReturnDTO = rentalServices.returnCopy(1159, returnDate1.plusDays(6));
        log.debug(rentalReturnDTO.toString());
        rentalReturnDTO = rentalServices.returnCopy(1111, returnDate1.plusDays(16));
        rentalReturnDTO = rentalServices.returnCopy(1225, returnDate1.plusDays(27));
        log.debug(rentalReturnDTO.toString());
        
        return rentals;
    }

    public GeneralSettings initializeGeneralSettings() {
        GeneralSettings generalSettings =
                new GeneralSettings(50, 8, 10, 20);
        
        generalSettingsServices.registerNewSettingsProfile(generalSettings);

        return generalSettings;
    }
}