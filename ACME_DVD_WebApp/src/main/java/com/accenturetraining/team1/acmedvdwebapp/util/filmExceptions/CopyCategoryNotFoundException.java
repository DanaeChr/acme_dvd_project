package com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Copy Category Not Found") //404
public class CopyCategoryNotFoundException extends IndexOutOfBoundsException {
    public CopyCategoryNotFoundException(long id) {

        super("CopyCategoryNotFoundException with id="+id);
    }
}
