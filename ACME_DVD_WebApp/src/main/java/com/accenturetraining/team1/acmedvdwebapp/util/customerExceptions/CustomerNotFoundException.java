package com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Customer Not Found") //404
public class CustomerNotFoundException extends IndexOutOfBoundsException {
    
    public CustomerNotFoundException(long id) {
        super("CustomerNotFoundException with id="+id);
    }
    
    public CustomerNotFoundException() {
        super("CustomerNotFoundException with the details provided");
    }
}