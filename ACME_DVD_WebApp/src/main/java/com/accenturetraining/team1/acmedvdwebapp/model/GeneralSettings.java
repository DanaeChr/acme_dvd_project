package com.accenturetraining.team1.acmedvdwebapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "general_settings")
@NoArgsConstructor
public class GeneralSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private double debtThreshold;
    private int numberOfDaysDelayedToNotifyCustomer;
    private int firstNumberOfDaysLateThreshold;
    private int secondNumberOfDaysLateThreshold;
    
    public GeneralSettings(double debtThreshold, int numberOfDaysDelayedToNotifyCustomer,
                           int firstNumberOfDaysLateThreshold, int secondNumberOfDaysLateThreshold) {
        this.debtThreshold = debtThreshold;
        this.numberOfDaysDelayedToNotifyCustomer = numberOfDaysDelayedToNotifyCustomer;
        this.firstNumberOfDaysLateThreshold = firstNumberOfDaysLateThreshold;
        this.secondNumberOfDaysLateThreshold = secondNumberOfDaysLateThreshold;
    }
}
