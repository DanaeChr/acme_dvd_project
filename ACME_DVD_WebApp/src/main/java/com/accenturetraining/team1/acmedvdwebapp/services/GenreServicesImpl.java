package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.model.Genre;
import com.accenturetraining.team1.acmedvdwebapp.repositories.FilmRepository;
import com.accenturetraining.team1.acmedvdwebapp.repositories.GenreRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class GenreServicesImpl implements GenreServices {
    @Autowired
    GenreRepository genreRepository;
    @Autowired
    FilmRepository filmRepository;
    
    @Override
    public Genre registerGenre(String genreName) {
        Genre genre = genreRepository.getByGenreName(genreName).orElse(null);
        
        if (genre == null) {
            genre = new Genre(genreName);
        }
        
        genre = genreRepository.save(genre);
        return genre;
    }
    
   
}
