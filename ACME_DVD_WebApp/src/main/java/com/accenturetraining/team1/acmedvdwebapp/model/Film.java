package com.accenturetraining.team1.acmedvdwebapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@Entity (name = "films")
//@JsonDeserialize(using = FilmDataDeserializer.class)
public class Film {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique = true)
    private long id;
    @NotNull (message = "The title of the film must not be null")
    @NotEmpty (message = "The title of the film must not be empty")
    private String title;
    @NotNull (message = "The release date of the film must not be null")
    private LocalDate releaseDate;
    @ToString.Exclude
    @OneToMany(mappedBy = "film")
    private List <Copy> copies;
    private boolean isAvailable;
    @Size(max = 100)
    private String director;
    @JsonIgnore
    @ToString.Exclude
    @ManyToMany(mappedBy = "films")
    private List< Actor > actors;
    @JsonIgnore
    @ToString.Exclude
    @ManyToMany(mappedBy = "films", cascade = CascadeType.ALL)
    private List<Genre> genres;
    @Size(max = 1000)
    private String description;

    public Film(String title, LocalDate releaseDate, String director,
                String genre, String description) {
        this.director = director;
        this.description = description;

    }
    
    public Film(long id, String title, LocalDate releaseDate, String director) {
        this.id = id;
        this.title = title;
        this.releaseDate = releaseDate;
        this.director = director;
    }
    
//    public Film(long id, String title, List<Genre> genres, String director, LocalDate releaseDate,
//                String description, List<Actor> actors) {
//        this.genres = genres;
//        this.director = director;
//        this.description = description;
//        this.actors = actors;
//    }
}
