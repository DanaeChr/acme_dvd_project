package com.accenturetraining.team1.acmedvdwebapp.repositories;

import com.accenturetraining.team1.acmedvdwebapp.dtos.FilmDetailsDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface FilmRepository extends JpaRepository<Film, Long> , JpaSpecificationExecutor < FilmDetailsDTO > {
    Optional<Film> getById(long id);
    Optional<List<Film>> getByTitleEquals(String title);
    
    Optional<List<Film>> findAllByTitleContaining(String titlePart);

    Optional<List<Film>> findAllByActorsLastNameContaining (String actorLastNamePart);

    Optional<List<Film>> findAllByReleaseDateBetween(LocalDate dateFrom, LocalDate dateTo);
    
    @Query(nativeQuery = true, value =
            "SELECT DISTINCT f.* " +
            "FROM films f " +
            "INNER JOIN genre_film gf ON gf.film_id = f.id  " +
            "INNER JOIN genres g ON g.id = gf.genre_id  " +
            "WHERE g.genre_name LIKE %:genreNamePart% " +
            "ORDER BY f.id")
    Optional<List<Film>> searchFilmsByGenre(String genreNamePart);
    
    @Query(nativeQuery = true, value =
            "SELECT DISTINCT f.* " +
            "FROM films f " +
            "INNER JOIN actor_film af ON af.film_id = f.id  " +
            "INNER JOIN actors a ON a.id = af.actor_id  " +
            "WHERE a.first_name LIKE %:actorFirstNamePart% " +
            "AND a.last_name LIKE %:actorLastNamePart% " +
            "ORDER BY f.id")
    Optional<List<Film>> searchFilmsByActor(String actorFirstNamePart, String actorLastNamePart);
    
    @Query(nativeQuery = true, value =
            "SELECT DISTINCT f.* " +
            "FROM films f " +
            "INNER JOIN copies c ON c.film_id = f.id  " +
            "WHERE c.is_rented=1 " +
            "ORDER BY f.id")
    Optional<List<Film>> findRentedFilms();
    
//    @Query(nativeQuery = true, value =
//            "SELECT f.id, f.title, f.release_date, COUNT(r.id) rental_count " +
//            "FROM films f " +
//            "INNER JOIN copies c ON c.film_id = f.id " +
//            "INNER JOIN rentals r ON r.id = c.id " +
//            "GROUP BY f.id, f.title, f.release_date " +
//            "ORDER BY COUNT(r.id), f.release_date")
//    List<FilmRentalCountDTO> findBestSellingFilms();
}
