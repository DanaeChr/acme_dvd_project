package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * Data Transfer Object
 * Used as a response body which counts the rentals of a film
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilmRentalCountDTO {
    private long id;
    private String title;
    private LocalDate releaseDate;
    private long rentalCount;
}
