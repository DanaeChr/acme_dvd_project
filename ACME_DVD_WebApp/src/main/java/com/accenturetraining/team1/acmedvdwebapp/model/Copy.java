package com.accenturetraining.team1.acmedvdwebapp.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity (name = "copies")
public class Copy {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name="film_id")
    private Film film;
    private boolean isRented;

    @ToString.Exclude

    @OneToMany(mappedBy = "copy", fetch = FetchType.EAGER)
    private List<Rental> rentals;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id")
    private CopyCategory copyCategory;
}
