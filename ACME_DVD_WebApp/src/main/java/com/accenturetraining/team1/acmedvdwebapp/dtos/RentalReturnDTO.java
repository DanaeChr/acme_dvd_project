package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data Transfer Object
 * Used as a response body to inform that the copy return was successful
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RentalReturnDTO {
    private long customerId;
    private long copyId;
    private long daysLate;
    private double totalCharge;
    private double updatedCustomerDebt;
    
    @Override
    public String toString() {
        return "RentalReturnDTO{The customer with the customerId=" + customerId
                + " returned the copy with copyId=" + copyId + " after " + daysLate
                + " days and was charged " + totalCharge + "€.\n" +
                " The updatedCustomerDebt is " + updatedCustomerDebt + "€.}";
    }
}