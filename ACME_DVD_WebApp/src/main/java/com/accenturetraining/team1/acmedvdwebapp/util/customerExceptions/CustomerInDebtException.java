package com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions;

public class CustomerInDebtException extends Exception {
    public CustomerInDebtException(long id) {
        super("CustomerInDebt with id="+id);
    }
}
