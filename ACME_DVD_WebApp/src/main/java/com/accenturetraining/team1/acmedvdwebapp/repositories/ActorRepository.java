package com.accenturetraining.team1.acmedvdwebapp.repositories;

import com.accenturetraining.team1.acmedvdwebapp.model.Actor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ActorRepository extends JpaRepository<Actor, Long> {
    Optional<List<Actor>> findAllByFilms(long filmId);

//    Optional<List<Actor>> findAllByFirstNameOrLastNameContaining(String actorNamePart);
    
    Optional<Actor> getByFirstNameAndLastName(String firstName, String LastName);
}
