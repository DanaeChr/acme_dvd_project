package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.ActorDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.Actor;

public interface ActorServices {

    /**
     * Register a new actor by first name and last lame
     * using a DTO for mapping the attributes with the model
     * @param actorDTO
     * @return actor
     */
    Actor registerNewActor(ActorDTO actorDTO);

    /**
     * Used to update the actor
     * @param actor
     */
    void updateActor(Actor actor);
}
