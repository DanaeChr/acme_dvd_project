package com.accenturetraining.team1.acmedvdwebapp.controllers;

import com.accenturetraining.team1.acmedvdwebapp.dtos.*;
import com.accenturetraining.team1.acmedvdwebapp.services.CopyServices;
import com.accenturetraining.team1.acmedvdwebapp.services.FilmServices;
import com.accenturetraining.team1.acmedvdwebapp.services.RentalServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/films")
public class FilmController {
    @Autowired
    FilmServices filmServices;
    @Autowired
    CopyServices copyServices;
    @Autowired
    RentalServices rentalServices;
    
    @GetMapping
    public ResponseEntity<List<FilmDetailsDTO>> getAllFilms() {
        List<FilmDetailsDTO> filmDetailsDTOS =  filmServices.getAllFilms();
        if (filmDetailsDTOS.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(filmDetailsDTOS, HttpStatus.OK);
    }
    
    @PostMapping(produces = "application/json", consumes = "application/json")
    @ResponseBody
    public ResponseEntity<?> registerNewFilm(@RequestBody FilmRequestBodyDTO filmRequestBodyDTO) { // add @Valid
        long id  = filmServices.registerNewFilm(filmRequestBodyDTO);
        String createdMessage= "The film with id " + id + " was created";
        GenericResponseDTO genericResponseDTO = new GenericResponseDTO(id, createdMessage);
        return new ResponseEntity<>(genericResponseDTO, HttpStatus.CREATED);
    }
    
    @PostMapping(value = "/{filmId}/copies/{copyCategoryId}")
    public ResponseEntity<?> registerNewCopy(@PathVariable long filmId,
                                             @PathVariable int copyCategoryId) {
        long copyId = copyServices.registerNewCopy(filmId, copyCategoryId);
        String createdMessage= "The copy with id=" + copyId + " of the film with id=" +
                filmId + " was created";
        GenericResponseDTO genericResponseDTO = new GenericResponseDTO(copyId, createdMessage);
        return new ResponseEntity<>(genericResponseDTO, HttpStatus.CREATED);
    }
    
    @PutMapping(value = "/{filmId}")
    public ResponseEntity<?> updateFilm(@PathVariable long filmId,
                                        @RequestBody FilmRequestBodyDTO filmRequestBodyDTO) {
        filmServices.editFilm(filmId, filmRequestBodyDTO);
        String createdMessage = "The film with id " + filmId + " was updated";
        GenericResponseDTO genericResponseDTO = new GenericResponseDTO(filmId, createdMessage);
        return new ResponseEntity<>(genericResponseDTO, HttpStatus.OK);
    }
    
    @GetMapping(value = "/{filmId}")
    public ResponseEntity<?> getFilm(@PathVariable Long filmId) {
        FilmDetailsDTO filmDetailsDTO = filmServices.getFilmDetails(filmId);
        return new ResponseEntity<>(filmDetailsDTO, HttpStatus.OK);
    }
    
    @GetMapping(value = "/unavailablefilms")
    @ResponseBody
    public ResponseEntity<?> getAllUnavailableFilms () {
        List<FilmDetailsDTO> filmDetailsDTOS = filmServices
                .getAllUnavailableFilms();
        return new ResponseEntity<>(filmDetailsDTOS, HttpStatus.OK);
    }
    
    @GetMapping(value = "/bestsellingfilms")
    @ResponseBody
    public ResponseEntity<?> getBestSellingFilms () {
        List<FilmRentalCountDTO> filmRentalCountDTOS = filmServices
                .getBestSellingFilms();
        return new ResponseEntity<>(filmRentalCountDTOS, HttpStatus.OK);
    }
    
    @GetMapping(value = "/rentalsbetween")
    @ResponseBody
    public ResponseEntity<?> getRentalReportForPeriod (@RequestBody BetweenDatesDTO betweenDatesDTO) throws IOException {
        List<FilmRentalCountDTO> filmRentalCountDTOS = rentalServices
                .getRentalReportForPeriod(betweenDatesDTO);
        rentalServices.printRentalReportToExcel(filmRentalCountDTOS);
        return new ResponseEntity<>(filmRentalCountDTOS, HttpStatus.OK);
    }
    
    @GetMapping(value = "/titles")
    public ResponseEntity<?> searchForFilmsByTitlePart(@RequestBody String titlePart) {
        List<FilmDetailsDTO> filmDetailsDTOS = filmServices.findFilmsByTitlePart(titlePart);
        return new ResponseEntity<>(filmDetailsDTOS, HttpStatus.OK);
    }
    
    @GetMapping(value = "/genres")
    public ResponseEntity<?> searchForFilmsByGenreNamePart(@RequestBody String genreNamePart) {
        List<FilmDetailsDTO> filmDetailsDTOS = filmServices.findFilmsByGenre(genreNamePart);
        return new ResponseEntity<>(filmDetailsDTOS, HttpStatus.OK);
    }
    
    @GetMapping(value = "/actors")
    public ResponseEntity<?> searchForFilmsByActorNamePart(@RequestBody ActorDTO actorDTO) {
        String actorFirstNamePart = actorDTO.getFirstName();
        String actorLastNamePart = actorDTO.getLastName();
        List<FilmDetailsDTO> filmDetailsDTOS = filmServices
                .findFilmsByActor(actorFirstNamePart, actorLastNamePart);
        return new ResponseEntity<>(filmDetailsDTOS, HttpStatus.OK);
    }
    
    @GetMapping(value = "/dates")
    @ResponseBody
    public ResponseEntity<?> searchForFilmsByReleaseDate (@RequestBody BetweenDatesDTO betweenDatesDTO) {
        String dateFromText = betweenDatesDTO.getDateFromText();
        String dateToText = betweenDatesDTO.getDateToText();
        List<FilmDetailsDTO> filmDetailsDTOS = filmServices
                .findFilmsByReleaseDate(dateFromText, dateToText);
        return new ResponseEntity<>(filmDetailsDTOS, HttpStatus.OK);
    }
    
    @GetMapping(value = "/titles/available")
    public ResponseEntity<?> searchForAvailableFilmsByTitlePart(@RequestBody String titlePart) {
        List<FilmDetailsDTO> filmDetailsDTOS = filmServices.findAvailableFilmsByTitlePart(titlePart);
        return new ResponseEntity<>(filmDetailsDTOS, HttpStatus.OK);
    }

//    @GetMapping(value = "/genres/available")
//    public ResponseEntity<?> searchForFilmsByGenreNamePart(@RequestBody String genreNamePart) {
//        List<FilmDetailsDTO> filmDetailsDTOS = filmServices.findFilmsByGenre(genreNamePart);
//        return new ResponseEntity<>(filmDetailsDTOS, HttpStatus.OK);
//    }
//
//    @GetMapping(value = "/actors/available")
//    public ResponseEntity<?> searchForFilmsByActorNamePart(@RequestBody ActorDTO actorDTO) {
//        String actorFirstNamePart = actorDTO.getFirstName();
//        String actorLastNamePart = actorDTO.getLastName();
//        List<FilmDetailsDTO> filmDetailsDTOS = filmServices
//                .findFilmsByActor(actorFirstNamePart, actorLastNamePart);
//
//        return new ResponseEntity<>(filmDetailsDTOS, HttpStatus.OK);
//    }
//
//    @GetMapping(value = "/dates")
//    @ResponseBody
//    public ResponseEntity<?> searchForFilmsByReleaseDate (@RequestBody BetweenDatesDTO betweenDatesDTO) {
//        String dateFromText = betweenDatesDTO.getDateFromText();
//        String dateToText = betweenDatesDTO.getDateToText();
//        List<FilmDetailsDTO> filmDetailsDTOS = filmServices
//                .findFilmsByReleaseDate(dateFromText, dateToText);
//        return new ResponseEntity<>(filmDetailsDTOS, HttpStatus.OK);
//    }
    

}
