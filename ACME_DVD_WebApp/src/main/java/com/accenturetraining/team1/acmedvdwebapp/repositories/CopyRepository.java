package com.accenturetraining.team1.acmedvdwebapp.repositories;

import com.accenturetraining.team1.acmedvdwebapp.model.Copy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CopyRepository extends JpaRepository<Copy, Long> {
    Optional<Copy> getById(long id);
    
    // Finds one available copy of the given film and copy category.
    Optional<Copy> getFirstByIsRentedFalseAndFilmIdAndCopyCategoryId(long filmId, int copyCategoryId);
    Optional<Copy> getFirstByIsRentedFalseAndFilmId(long filmId);
    Optional<List<Copy>> findAllByRentalsNotNull();
}
