package com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDate;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Film Not Found") //404
public class FilmNotFoundException extends IndexOutOfBoundsException {
    
    public FilmNotFoundException(long id) {

        super("FilmNotFoundException with id="+id);
    }

    public FilmNotFoundException(String title) {
        super("FilmNotFoundException with title="+title);
    }
    
    public FilmNotFoundException(LocalDate dateFrom, LocalDate dateTo) {
        super("FilmNotFoundException released between " + dateFrom.toString()
                + " and " + dateTo);
    }
    
    public FilmNotFoundException() {
        super("FilmNotFoundException that is anavailable ");
    }
}
