package com.accenturetraining.team1.acmedvdwebapp.repositories;

import com.accenturetraining.team1.acmedvdwebapp.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {
    Optional<Genre> getByGenreName(String genreName);
}
