package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.PaymentDetailsDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.Customer;
import com.accenturetraining.team1.acmedvdwebapp.model.Payment;
import com.accenturetraining.team1.acmedvdwebapp.repositories.CustomerRepository;
import com.accenturetraining.team1.acmedvdwebapp.repositories.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class PaymentServicesImpl implements PaymentServices {
    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    CustomerServices customerServices;
    
    @Override
    public PaymentDetailsDTO makePayment(long customerId, double amount) {
        // Check if the customer exists.
        Customer customer = customerServices.findCustomerById(customerId);
        
        // Create a new Payment and connect it with customer, amount and current time.
        Payment payment = new Payment();
        payment.setAmount(amount);
        payment.setCustomer(customer);
        LocalDateTime paymentDateAndTime = LocalDateTime.now();
        payment.setPaymentDate(LocalDateTime.now());
        // Insert the payment to the DB.
        paymentRepository.save(payment);
        // Subtract amount from customer debt and update customer.
        double originalCustomerDebt = customer.getDebt();
        double originalCustomerTotalPayments = customer.getTotalPayments();
        double updatedCustomerDebt = originalCustomerDebt - amount;
        double updatedCustomerTotalPayments = originalCustomerTotalPayments + amount;
        customer.setDebt(updatedCustomerDebt);
        customer.setTotalPayments(updatedCustomerTotalPayments);
        customerRepository.save(customer);
        PaymentDetailsDTO paymentDetailsDTO = new PaymentDetailsDTO(customerId,
                paymentDateAndTime, amount, updatedCustomerDebt);
        return  paymentDetailsDTO;
    }
}
