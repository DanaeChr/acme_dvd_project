package com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Film Not Found") //404
public class FilmNotFoundForActorException extends IndexOutOfBoundsException {

    public FilmNotFoundForActorException(String actorFirstNamePart, String actorLastNamePart) {
        super("FilmNotFoundForGenreException for actor=" +
                actorFirstNamePart + " " + actorLastNamePart);
    }
}
