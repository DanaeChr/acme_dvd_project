package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data Transfer Object
 * Used for general use purposes, provides responses to requests
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenericResponseDTO {
    private long id;
    private String message;
}
