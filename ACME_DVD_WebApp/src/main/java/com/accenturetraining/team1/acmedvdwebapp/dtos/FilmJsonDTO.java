package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.Data;

import java.util.List;

/**
 * Data Transfer Object
 * Used for mapping legacy file to the model
 */

@Data
public class FilmJsonDTO {
    private long id;
    private String movieTitle;
    private String genre;
    private String director;
    private String year;
    private String description;
    private List<ActorDTO> actor;
}
