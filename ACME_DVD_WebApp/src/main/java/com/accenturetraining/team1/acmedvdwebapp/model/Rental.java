package com.accenturetraining.team1.acmedvdwebapp.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity (name = "rentals")
public class Rental {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false, updatable = false)
    private LocalDateTime rentalDate;
//    @Temporal(TemporalType.DATE)
    private LocalDateTime returnDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id", updatable = false, nullable = false)
    private Customer customer;

    private double totalCharge;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "copy_id", updatable = false, nullable = false)
    private Copy copy;
}
