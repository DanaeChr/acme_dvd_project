package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data Transfer Object which functions as a request body
 * for register and edit a customer
 */

@Data
@NoArgsConstructor
public class CustomerRequestBodyDTO {
    private String firstName;
    private String lastName;
    private String address;
    private String mobileNumber;
    private String homeNumber;
    private String email;
    private String identificationNumber;
}
