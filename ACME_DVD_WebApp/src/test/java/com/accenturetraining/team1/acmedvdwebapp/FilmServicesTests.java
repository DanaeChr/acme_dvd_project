package com.accenturetraining.team1.acmedvdwebapp;

import com.accenturetraining.team1.acmedvdwebapp.repositories.CopyCategoryRepository;
import com.accenturetraining.team1.acmedvdwebapp.services.CopyServices;
import com.accenturetraining.team1.acmedvdwebapp.services.FilmServices;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@Slf4j
@DataJpaTest
public class FilmServicesTests {
    @Autowired
    FilmServices filmServices;
    @Autowired
    CopyServices copyServices;
    @Autowired
    CopyCategoryRepository copyCategoryRepository;
    
    
//    @Test
//    public void testingRegisterNewFilm() throws FilmDuplicateException, FilmDataIncompleteException {
//        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("M/d/yyyy");
//        LocalDate date = LocalDate.parse("5/2/2020", dateFormat);
//        List<Film> films = new ArrayList<>();
//        Film film1 = new Film(1, "Luzhin Defence, The", LocalDate.now(), "Hill Lawry");
//        films.add(film1);
//        Film film2 = new Film(2, "Halloweentown High", LocalDate.now(), "Felicity Derycot");
//        films.add(film2);
//        Film film3 = new Film(3, "Halloweentown High", date, "Robert Redford");
//        films.add(film3);
//        try {
//            filmServices.registerNewFilm(film1);
//            filmServices.registerNewFilm(film2);
//            filmServices.registerNewFilm(film3);
//        }
//        catch (FilmDataIncompleteException e) {
//            e.printStackTrace();
//        }
//        catch (FilmDuplicateException e) {
//            e.printStackTrace();
//        }
//
//        CopyCategory dvd = new CopyCategory();
//        dvd.setCategoryName("DVD");
//        dvd.setInitialFee(0.5);
//        dvd.setDailyLateFee(0.25);
//        dvd.setFirstPenalty(10);
//        dvd.setSecondPenalty(20);
//        copyCategoryRepository.save(dvd);
//        for(Film film : films)
//        {
//            for(int i = 0; i < 4; i++) {
//                copyServices.registerNewCopy(film, dvd);
//            }
//        }
//    }
}
